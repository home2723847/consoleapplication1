﻿#include <iostream>
#include <string_view>

int main()
{
    std::cout << "Enter string  ";
    std::string str;
    std::cin >> str;

    std::cout << "Length" << " " << str.length() << "\n";
    std::cout <<"First sumbol" << " " << str.front() << "\n";    
    std::cout << "Last sumbol" << " " << str.back() << "\n";

}